// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngStorage','ngCordova','google.places','starter.factories'])

.run(function($ionicPlatform,$rootScope,$state,SendPostToServer,$ionicPopup,$localStorage) {
	
$rootScope.LaravelHost = 'http://everest.bnyah.co.il/laravel/public/';
$rootScope.serverHost = 'http://everest.bnyah.co.il/php/';

$rootScope.currState = $state;
$rootScope.State = '';
$rootScope.settingsArray = [];
$rootScope.previousTab = "";
$rootScope.Mapinterval = "";
//$rootScope.defaultMapZoom = 13;
//$rootScope.defaultMapLat = "32.791698";
//$rootScope.defaultMapLng = "34.989996";
//$rootScope.mapStyleDefault = 'google.maps.MapTypeId.ROADMAP';
$rootScope.showLoading = 1;


$rootScope.$watch('currState.current.name', function(newValue, oldValue) {
  $rootScope.State = newValue;
}); 

	
	
  $ionicPlatform.ready(function() {
	  
	  
	  
	$rootScope.getSettings = function()
	{
		$rootScope.params = {};
		SendPostToServer($rootScope.params,$rootScope.LaravelHost+'/GetClientSettings',function(data, success) 
		{					
			$rootScope.settingsArray = data;
			console.log("settings: " , data);
		});			
	}
	
	$rootScope.getSettings();
	
	$rootScope.loginfields = 
	{
		"username" : "",
		"password" : "",
	}	
	
	$rootScope.doLogin = function()
	{
		
		if ($rootScope.loginfields.username =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין שם משתמש',
			 template: ''
			});				
		}
		else if ($rootScope.loginfields.password =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין סיסמה',
			 template: ''
			});				
		}
		else
		{
			
			$rootScope.sendfields = 
			{
				"username" : $rootScope.loginfields.username,
				"password" : $rootScope.loginfields.password,
				"pushid" : typeof $rootScope.pushId == "undefined" ? '000' : $rootScope.pushId, // "0877c1e6-45d5-43c3-b90f-483d599f7e4d"
				"timestamp" : window.localStorage.timestamp
			}
			SendPostToServer($rootScope.sendfields,$rootScope.LaravelHost+'/UserLogin',function(data, success) 
			{
				//alert(JSON.stringify(data));
					
				if (data.status == 0)
				{
					$ionicPopup.alert({
					 title: 'שם משתמש או סיסמה שגוים יש לנסות שוב',
					 template: ''
					});		

					$rootScope.loginfields.password = '';
				}
				else
				{
					if (data.userid)
					{
						window.localStorage.userid = data.userid;
						window.localStorage.name = data.name;
						window.localStorage.deliverytime = data.delivery_time;
						
					
						window.localStorage.usertype = data.type; // 0 = company,1 = deliveryman, 2 = admin, 3 = private user
						window.localStorage.image = data.image;
						
						window.localStorage.phone = data.phone
						window.localStorage.address = data.address
						window.localStorage.city = data.city
						window.localStorage.house_number = data.house_number
						window.localStorage.floor = data.floor
						window.localStorage.apartment = data.apartment
						window.localStorage.email = data.email;
						window.localStorage.backgroundLocation = data.locationEnabled;
						$rootScope.loginfields.username = '';
						$rootScope.loginfields.password = '';
						

						//if (data.type == 0)
							window.location ="#/app/companymain";
							$rootScope.checkUserConnected();						
					}
					else
					{
						$ionicPopup.alert({
						 title: 'שגיאה בהתחברות יש לבדוק חיבור לאינטרנט',
						 template: ''
						});							
					}

					
				}
			});	
		}
	}


  var notificationOpenedCallback = function(jsonData) {

  //alert(JSON.stringify(jsonData.payload.additionalData));
  
  if (jsonData.payload.additionalData.type == "newchatmsg")
  {
	  if (!jsonData.isAppInFocus)
      {
		  $rootScope.$broadcast('movetab',2);
	  }
	  $rootScope.$broadcast('newchatmsg',jsonData.payload.additionalData);
  }
  
  if (jsonData.payload.additionalData.type == "newadminchatmsg")
  {
	  if (!jsonData.isAppInFocus)
	  {
		  window.location ="#/app/chat/"+jsonData.payload.additionalData.userid;

	  }
	  $rootScope.$broadcast('newadminchatmsg',jsonData.payload.additionalData);
  }  

  if (jsonData.payload.additionalData.type == "deliverytimeupdate")
  {
	   $rootScope.$broadcast('deliverytimeupdate',jsonData.payload.additionalData);
  }



  if (jsonData.payload.additionalData.type == "newprivateorder")
  {
	   $rootScope.$broadcast('newprivateorder',jsonData.payload.additionalData);
  }

  

    //console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
  };
  


  if (window.cordova)
  {
	  
	
	document.addEventListener('deviceready', function() {

		window.plugins.OneSignal
			.startInit("bf525572-86e6-48b6-9198-6ae09790d3a8")
			.inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.None)			
			.handleNotificationReceived(notificationOpenedCallback)
			.handleNotificationOpened(notificationOpenedCallback)
			.endInit();

		window.plugins.OneSignal.getIds(function (ids) {
			$rootScope.pushId = ids.userId;
		});
		
		//OneSignal.clearOneSignalNotifications();
		//window.plugins.OneSignal.clearOneSignalNotifications();

		
	  /*
	  window.plugins.OneSignal.init("bf525572-86e6-48b6-9198-6ae09790d3a8",
									 {googleProjectNumber: "595323574268"},
									 notificationOpenedCallback);

	  window.plugins.OneSignal.getIds(function(ids) {
		
	  $rootScope.pushId = ids.userId;	
	  
	  });
	  */
									 
	  // Show an alert box if a notification comes in when the user is in your app.
	  //window.plugins.OneSignal.enableInAppAlertNotification(false);
	  window.plugins.OneSignal.enableNotificationsWhenActive(false);
	  
	  
	
	});  
	

	
	  
  }




  
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	$ionicConfigProvider.backButton.previousTitleText(false).text('');
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller: 'LoginRegisterCtrl'
      }
    }
  })

  .state('app.register', {
    url: '/register',
    views: {
      'menuContent': {
        templateUrl: 'templates/register.html',
        controller: 'LoginRegisterCtrl'
      }
    }
  }) 
  
  .state('app.updateinfo', {
    url: '/updateinfo',
    views: {
      'menuContent': {
        templateUrl: 'templates/updateinfo.html',
        controller: 'UpdateInfoCtrl'
      }
    }
  })  
  
  
  .state('app.forgotpass', {
    url: '/forgotpass',
    views: {
      'menuContent': {
        templateUrl: 'templates/forgotpass.html',
        controller: 'LoginRegisterCtrl'
      }
    }
  })   
  
 
  .state('app.companymain', {
    url: '/companymain',
    views: {
      'menuContent': {
        templateUrl: 'templates/companymain.html',
        controller: 'MainCtrl'
      }
    }
  })


  .state('app.chat', {
    url: '/chat/:ItemId',
    views: {
      'menuContent': {
        templateUrl: 'templates/chat.html',
        controller: 'ChatCtrl'
      }
    }
  })


  .state('app.mapadmin', {
    url: '/mapadmin',
    views: {
      'menuContent': {
        templateUrl: 'templates/mapadmin.html',
        controller: 'MapAdminCtrl'
      }
    }
  })

  
  .state('app.demo', {
    url: '/demo',
    views: {
      'menuContent': {
        templateUrl: 'templates/demo.html',
        controller: 'DemoCtrl'
      }
    }
  })


  


  .state('app.html_1', {
    url: '/html_1',
    views: {
      'menuContent': {
        templateUrl: 'templates/html_1.html',
        controller: 'MainCtrl'
      }
    }
  })

    .state('app.html_2', {
    url: '/html_2',
    views: {
      'menuContent': {
        templateUrl: 'templates/html_2.html',
        controller: 'MainCtrl'
      }
    }
  })

     .state('app.html_3', {
    url: '/html_3',
    views: {
      'menuContent': {
        templateUrl: 'templates/html_3.html',
        controller: 'MainCtrl'
      }
    }
  })

      .state('app.html_4', {
    url: '/html_4',
    views: {
      'menuContent': {
        templateUrl: 'templates/html_4.html',
        controller: 'MainCtrl'
      }
    }
  })

  .state('app.html_5', {
    url: '/html_5',
    views: {
      'menuContent': {
        templateUrl: 'templates/html_5.html',
        controller: 'MainCtrl'
      }
    }
  })

   .state('app.html_6', {
    url: '/html_6',
    views: {
      'menuContent': {
        templateUrl: 'templates/html_6.html',
        controller: 'MainCtrl'
      }
    }
  })

    .state('app.html_7', {
    url: '/html_7',
    views: {
      'menuContent': {
        templateUrl: 'templates/html_7.html',
        controller: 'MainCtrl'
      }
    }
  })

     .state('app.html_8', {
    url: '/html_8',
    views: {
      'menuContent': {
        templateUrl: 'templates/html_8.html',
        controller: 'MainCtrl'
      }
    }
  })

        .state('app.html_10', {
    url: '/html_10',
    views: {
      'menuContent': {
        templateUrl: 'templates/html_10.html',
        controller: 'MainCtrl'
      }
    }
  })
  

  
  
  ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
