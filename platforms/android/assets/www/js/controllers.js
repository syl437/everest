angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope,$rootScope,SendPostToServer, $ionicModal, $timeout,$localStorage,$ionicSideMenuDelegate,$http,$cordovaBackgroundGeolocation) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  
	$http.defaults.headers.post[

		"Content-Type"

		] = "application/x-www-form-urlencoded; charset=UTF-8; application/json";


	document.addEventListener(
		"deviceready",

		function () {
			
			if (window.localStorage.backgroundLocation == 1)
			{
				if(ionic.Platform.platform() !== 'ios') {
					var backgroundGeolocationConfig = 
					{

						desiredAccuracy: 0,
						stationaryRadius: 0,
						distanceFilter: 100,
						debug: false,
						stopOnTerminate: true
					};

					window.backgroundGeolocation.configure(
						function (location) {

							send_params = 
							{
								user: window.localStorage.userid,
								location_lat: location.latitude,
								location_lng: location.longitude
							};

							$http

								.post(
									$rootScope.LaravelHost+'/UpdateUserLocation',
									send_params
								)

								.success(function (data, status, headers, config) {
									console.log("success");
								})

								.error(function (data, status, headers, config) {
									console.log("error");
								});

						},

						function (error) {
						},

						backgroundGeolocationConfig
					);


					backgroundGeolocation.isLocationEnabled(function (enabled) {

						if (enabled) {

							backgroundGeolocation.start(
								function () {

									// service started successfully

									// you should adjust your app UI for example change switch element to indicate

									// that service is running

								},

								function (error) {

									// Tracking has not started because of error

									// you should adjust your app UI for example change switch element to indicate

									// that service is not running

									if (error.code === 2) {

										if (

											window.confirm(
												"Not authorized for location updates. Would you like to open app settings?"
											)

										) {
											backgroundGeolocation.showAppSettings();
										}

									} else {
										//window.alert("Start failed: " + error.message);
									}

								}
							);

						} else {

							// Location services are disabled

							if (

								window.confirm(
									"Location is disabled. Would you like to open location settings?"
								)

							) {

								backgroundGeolocation.showLocationSettings();

							}

						}

					});
				} else {
					console.log('configuring ios background geolocation');
					var bgGeo = window.BackgroundGeolocation;

					//This callback will be executed every time a geolocation is recorded in the background.
					var callbackFn = function (location, taskId) {
						var coords = location.coords;
						var lat = coords.latitude;
						var lng = coords.longitude;

						send_params = 
						{
							user: window.localStorage.userid,
							location_lat: lat,
							location_lng: lng
						};

						$http

							.post(
								$rootScope.LaravelHost+'/UpdateUserLocation',
								send_params
							)

							.success(function (data, status, headers, config) {
								console.log("success");
							})

							.error(function (data, status, headers, config) {
								console.log("error");
							});

						// Must signal completion of your callbackFn.
						bgGeo.finish(taskId);   
					};
   
					// This callback will be executed if a location-error occurs.  Eg: this will be called if user disables location-services.
					var failureFn = function (errorCode) {
						console.warn('- BackgroundGeoLocation error: ', errorCode);
					}

					// Listen to location events & errors.
					bgGeo.on('location', callbackFn, failureFn);

					var backgroundGeolocationConfig = {
						desiredAccuracy: 0,
						stationaryRadius: 0,
						distanceFilter: 100,
						debug: true,
						stopOnTerminate: false
					};

					bgGeo.configure(backgroundGeolocationConfig, function (state) {
						// This callback is executed when the plugin is ready to use.
						console.log("BackgroundGeolocation ready: ", state);
						if (!state.enabled) {
							bgGeo.start();
						}
					});
				}

				// window.backgroundGeoLocation.start();				
			}
		},
		false
	);
  
  
  $rootScope.checkUserConnected = function()
  {
	  if (window.localStorage.userid)
		  $scope.userConnected = true;
	  else
		  $scope.userConnected = false;
	  
	  $scope.usertype = window.localStorage.usertype ;
  }
  
  
  $rootScope.checkUserConnected();
  
  
  if (!window.localStorage.timestamp)
	window.localStorage.timestamp = Math.floor(Date.now() / 1000);
  
  
  $scope.RemoveUserPush = function()
  {
	$scope.sendparams = 
	{
		"user" : window.localStorage.userid,
		"pushid" : $rootScope.pushId,
		"timestamp" : window.localStorage.timestamp
	}
	
	SendPostToServer($scope.sendparams,$rootScope.LaravelHost+'/RemoveUserPush',function(data, success) 
	{	
		window.localStorage.userid = '';
		window.localStorage.name  = '';
		window.localStorage.usertype  = '';
		window.localStorage.image = '';
		window.localStorage.deliverytime = '';	
		window.localStorage.backgroundLocation = "0";		
		window.location ="#/app/login";	  
		$rootScope.checkUserConnected();
		//$ionicSideMenuDelegate.toggleRight();
		
	});	
  }
  
  $scope.logOut = function()
  {
	$scope.RemoveUserPush();
  }
})

.controller('LoginRegisterCtrl', function($scope, $stateParams,$ionicPopup,$localStorage,$rootScope,SendPostToServer,$ionicHistory,$state,$ionicPlatform,$http) {




    if (window.localStorage.userid)
    {
        $ionicHistory.nextViewOptions({
                disableAnimate: true,
                expire: 300,
				disableBack: true
            });
    
        $state.go('app.companymain');
    }

	$rootScope.showLoading = 1;
	
	$scope.loginfields2 = 
	{
		"username" : "",
		"password" : "",
	}
	
	$scope.registerfields = 
	{
		"name" : "",
		"city" : "",
		"address" : "",
		"fulladdress" : "",
		"house_number" : "",
		"floor" : "",
		"apartment" : "",
		"phone" : "",
		"username" : "",
		"password" : "",
		"email" : "",
		"location_lat" : "",
		"location_lng" : "",
		"pushid" : $rootScope.pushId,
		"timestamp" : window.localStorage.timestamp

	}


	$scope.forgotfields = 
	{
		"phone" : ""
	}
	
	$scope.autocompleteOptions = 
	{
	  //componentRestrictions: { country: 'IL' }
	}	

	
	
	$scope.doRegister = function()
	{
		

		var emailRegex = /\S+@\S+\.\S+/;
		
		if ($scope.registerfields.name =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין שם לקוח',
			 template: ''
			});					
		}
		else if ($scope.registerfields.city =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין עיר',
			 template: ''
			});					
		}
		else if ($scope.registerfields.address =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין כתובת',
			 template: ''
			});					
		}
		else if ($scope.registerfields.house_number =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין מספר בית',
			 template: ''
			});					
		}
		else if ($scope.registerfields.floor =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין קומה',
			 template: ''
			});					
		}
		else if ($scope.registerfields.apartment =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין דירה',
			 template: ''
			});					
		}
		else if ($scope.registerfields.phone =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין טלפון',
			 template: ''
			});					
		}
		else if ($scope.registerfields.username =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין שם משתמש',
			 template: ''
			});					
		}
		else if ($scope.registerfields.password =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין סיסמה',
			 template: ''
			});					
		}

		else if ($scope.registerfields.email !="" && emailRegex.test($scope.registerfields.email) == false)
		{

			$ionicPopup.alert({
			title: 'דוא"ל לא תקין יש לתקן',
			template: '',		
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	

		   $scope.registerfields.email =  '';
		}

		
		else
		{
			
			
			if ($scope.registerfields.address.formatted_address)
			{
				$scope.newaddressgeo = String($scope.registerfields.address.geometry.location);
				$scope.splitaddress = $scope.newaddressgeo.split(",");
				$scope.registerfields.location_lat = $scope.splitaddress[0].replace("(", "");
				$scope.registerfields.location_lng = $scope.splitaddress[1].replace(")", "");
				$scope.registerfields.fulladdress = $scope.registerfields.address.formatted_address;				
				//$scope.fulladdress = $scope.registerfields.address.formatted_address;
	
			}
			else
				$scope.registerfields.fulladdress = $scope.registerfields.address;
				//$scope.fulladdress = $scope.fields.address;



			SendPostToServer($scope.registerfields,$rootScope.LaravelHost+'/RegisterPrivateUser',function(data, success) 
			{
					
				if (data.status == 0)
				{
					
					window.localStorage.userid = data.userid;
					window.localStorage.name  = $scope.registerfields.name;
					window.localStorage.phone = $scope.registerfields.phone;
					window.localStorage.address = $scope.registerfields.fulladdress;
					window.localStorage.city = $scope.registerfields.city;
					window.localStorage.house_number = $scope.registerfields.house_number;
					window.localStorage.floor = $scope.registerfields.floor;
					window.localStorage.apartment = $scope.registerfields.apartment;
					window.localStorage.email = $scope.registerfields.email;
					window.localStorage.usertype  = 3; // 0 = company,1 = deliveryman, 2 = admin, 3 = private user
					//window.localStorage.image = data.image;
					

					$scope.registerfields.name = '';
					$scope.registerfields.city = '';
					$scope.registerfields.address = '';
					$scope.registerfields.house_number = '';
					$scope.registerfields.floor = '';
					$scope.registerfields.apartment = '';
					$scope.registerfields.phone = '';
					$scope.registerfields.username = '';
					$scope.registerfields.password = '';
					$scope.registerfields.email = '';
					$scope.registerfields.location_lat = '';
					$scope.registerfields.location_lng = '';
					window.location ="#/app/companymain";
					$rootScope.checkUserConnected();

						
				}
				else if (data.status == 1)
				{
					
					$ionicPopup.alert({
					 title: 'שם משתמש כבר קיים יש להזין שם משתמש אחר',
					 template: ''
					});		
				}

			});				
		}
	}

  $scope.$on('$ionicView.enter', function(e) {
	  
	//$ionicPlatform.ready(function() {

	$scope.doLogin2 = function()
	{
		
		if ($scope.loginfields.username =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין שם משתמש',
			 template: ''
			});				
		}
		else if ($scope.loginfields.password =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין סיסמה',
			 template: ''
			});				
		}
		else
		{
			
			$scope.sendfields = 
			{
				"username" : $scope.loginfields.username,
				"password" : $scope.loginfields.password,
				"pushid" : $rootScope.pushId, // "0877c1e6-45d5-43c3-b90f-483d599f7e4d"			
				"timestamp" : window.localStorage.timestamp
			}
			SendPostToServer($scope.sendfields,$rootScope.LaravelHost+'/UserLogin',function(data, success) 
			{
				//alert(JSON.stringify(data));
					
				if (data.status == 0)
				{
					$ionicPopup.alert({
					 title: 'שם משתמש או סיסמה שגוים יש לנסות שוב',
					 template: ''
					});		

					$scope.loginfields.password = '';
				}
				else
				{
					if (data.userid)
					{
						window.localStorage.userid = data.userid;
						window.localStorage.name  = data.name;
						window.localStorage.deliverytime = data.delivery_time;
						
					
						window.localStorage.usertype  = data.type; // 0 = company,1 = deliveryman, 2 = admin, 3 = private user
						window.localStorage.image = data.image;
						
						window.localStorage.phone = data.phone
						window.localStorage.address = data.address
						window.localStorage.city = data.city
						window.localStorage.house_number = data.house_number
						window.localStorage.floor = data.floor
						window.localStorage.apartment = data.apartment
						window.localStorage.email = data.email;
						window.localStorage.backgroundLocation = data.locationEnabled;
						
						$scope.loginfields.username = '';
						$scope.loginfields.password = '';

						
						
						//if (data.type == 0)
							window.location ="#/app/companymain";
							$rootScope.checkUserConnected();						
					}
					else
					{
						$ionicPopup.alert({
						 title: 'שגיאה בהתחברות יש לבדוק חיבור לאינטרנט',
						 template: ''
						});							
					}

					
				}
			});	
		}
	}

	
	
	//});

	  
	  
	  
	  
  });	
	

	
	$scope.sendPassword = function()
	{
		if ($scope.forgotfields.phone =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין מספר טלפון',
			 template: ''
			});					
		}
		else
		{
			SendPostToServer($scope.forgotfields,$rootScope.LaravelHost+'/ForgotPassword',function(data, success) 
			{
				if (data.status == 0)
				{
					$ionicPopup.alert({
					 title: 'מספר טלפון לא קיים במערכת יש לנסות שוב',
					 template: ''
					});	
			
					$scope.forgotfields.phone = '';
				}
				else
				{
					$ionicPopup.alert({
					 title: 'סיסמה נשלחה בהצלחה',
					 template: ''
					});	
					
					$scope.forgotfields.phone = '';
					window.location ="#/app/login";
				}
			});
		}
	}
	
	
})


.controller('MainCtrl', function($scope, $stateParams,$ionicPopup,$localStorage,$rootScope,SendPostToServer,$ionicScrollDelegate,$timeout,$ionicModal,$ionicPlatform,$cordovaCamera,$ionicHistory,$state,$cordovaGeolocation,$interval,$ionicLoading) {

    if (!window.localStorage.userid)
    {
        $ionicHistory.nextViewOptions({
                disableAnimate: true,
                expire: 300,
				disableBack: true
            });
    
        $state.go('app.login');
    }
	
	

	$scope.navTitle = "<p dir='rtl'>"+window.localStorage.name ;+"</p>"
	$scope.host = $rootScope.serverHost;
	$scope.deliveryTime = "";
	$scope.informationArray = [];
	$scope.deliveryInfoArray = "";
	$scope.deliveryTime = window.localStorage.deliverytime;

	
	
	$rootScope.$watch('settingsArray', function () 
	{   
		$scope.informationArray = $rootScope.settingsArray;
		
		//if ($rootScope.settingsArray[0])
			//$scope.deliveryTime = $rootScope.settingsArray[0].delivery_time;
	});

	
	
	$scope.locationx = "";
	$scope.locationy = "";
	$scope.DeliverysArray = [];
	$scope.chatArray = new Array();
	$scope.user_local_storage = window.localStorage.userid;
	$scope.user_type = window.localStorage.usertype ;
	$scope.DeliveryStatusTab = 0;
	$scope.unseenDeliveries = 0;
	$scope.uncompleteRegularDeliveries = 0;
	$scope.uncompletePrivateDeliveries = 0;
	$scope.currentAdminChatTab = 0;
	$scope.deliveryPopupType = '';
	$scope.deliveryIndex = '';
	$scope.checkPhoneExists = 0;
	$scope.unreadChatMessages = 0;
	$scope.countUnreadDeliveryman = 0;
	$scope.countUnreadResturants = 0;	
	$scope.countUnreadPrivateCustumer = 0;	
	$scope.openTimePopupType = '';
	$scope.googlePlacesFound = false;
	$rootScope.showLoading = 1;
	$scope.privateCustumerSaveType = '';

	
	if ($scope.user_type == 0 )
		$scope.ActiveTab = 0;
	else if ($scope.user_type == 1 || $scope.user_type == 2)
		$scope.ActiveTab = 1;
	else if ($scope.user_type == 3)
		$scope.ActiveTab = 3;
	
	if ($rootScope.previousTab)
	{
		$scope.ActiveTab = 2;
		$rootScope.previousTab = '';
	}
		

	$scope.setActiveTab = function(tab)
	{
		//$scope.DeliverysArray = [];
		$scope.DeliveryStatusTab = 0;
		$scope.ActiveTab = tab;
		
		if (tab != 2)
			$ionicScrollDelegate.scrollTop();
	}
	
	$scope.changeTabDelivery = function(tab)
	{
		$scope.DeliveryStatusTab = tab;
	}
	
	$scope.changeChatTab = function(tab)
	{
		$scope.currentAdminChatTab = tab;
		//console.log("messages: " , $scope.AdminMessagesArray)
	}

	
	$scope.autocompleteOptions = 
	{
	  //componentRestrictions: { country: 'IL' }
	}	
	
	$scope.fields = 
	{
		"address" : "",
		"location_lat" : "",
		"location_lng" : "",
		"city" : "",
		"street" : "",
		"streetnumber" : "",
		"phone" : "",
		"housenumber" : "",
		"chatbox" : ""
	}


	$scope.deliveryfields = 
	{
		"index" : "",
		"id" : "",
		"time" : "",
		"deliveryman" : "",
		"previousdeliveryman" : "",
		"newdeliverymanname" : "",
		"deliverytime" : "",
		"company_id" : "",
		"timeupdated" : ""
	}
	
	$scope.orderfields = 
	{
		"details": "",
		"desc" : ""
	}
	
	$scope.privatedelivery = 
	{
		"user" : window.localStorage.userid,
		"index" : "",
		"id" : "",
		"time" : "",
		"price" : "",
		"recipent" : "",
		"pushid" : $rootScope.pushId
	}
	
	$scope.pushprivatedelivery = 
	{
		"enabled" : false,
		"id" : "",
		"time" : "",
		"price" : ""
	}
	

	$scope.$watch('ActiveTab', function () 
	{   
		if ($scope.ActiveTab == 1)
			$scope.GetDeliveries(0);
		
		if ($scope.ActiveTab == 2 && $scope.user_type != 2)
			$scope.getChatHistory();	
		
		if ($scope.ActiveTab == 2 && $scope.user_type == 2)
			$scope.getAdminMessages();
		

		if ($scope.ActiveTab == 2 && $scope.user_type != 2)
			$scope.unreadChatMessages = 0;

		if ($scope.ActiveTab == 4 && $scope.user_type == 2)
			$scope.GetDeliveries(1);
		
		if ($scope.ActiveTab == 3 && $scope.user_type == 3)
			$scope.getPrivateDeliveryResponse();
		
	});	
	
	
	$scope.doRefresh = function()
	{
		$scope.GetDeliveries(0);
		$scope.$broadcast('scroll.refreshComplete');
	}
	


	$rootScope.$on('canceldelivery', function(event, args) {
	
		if ($scope.ActiveTab == 1)
			$scope.GetDeliveries(0);
		
	});	

	
	$rootScope.$on('deliverytimeupdate', function(event, args) {

	
		 $timeout(function() {
			if (args.isnew == 1)
				$scope.uncompleteRegularDeliveries++;
		}, 300);		

		
		if ($scope.ActiveTab == 1)
			$scope.GetDeliveries(0);
		
		 //notify the resturant of change in arrival time
		if (args.timeupdated == 1 && args.deliveryid)
		{

			if ($scope.DeliverysArray.length > 0)
			{
				for (var i = 0; i < $scope.DeliverysArray.length; i++) 
				{
					if ($scope.DeliverysArray[i].index == args.deliveryid)
					{
						$ionicPopup.alert({
						 title: 'המשלוח ל'+$scope.DeliverysArray[i].address+' התעדכן לשעה: '+args.newtime,
						 template: ''
						});							
						//$scope.DeliverysArray[i].timeupdated = 1;
					}		
				}
			}				
		}
	});

	$rootScope.$on('newprivateorder', function(event, args) {
		
		
		 $timeout(function() {
			if (args.isnew == 1)
				$scope.uncompletePrivateDeliveries++;
		}, 300);	

		
		if ($scope.ActiveTab == 3)
			$scope.getPrivateDeliveryResponse();	
		/*get response back for private custumer */
		
		/*
		 $timeout(function() {
			if (args.orderresponse == 1)
			{
				$scope.pushprivatedelivery.enabled = true;
				$scope.pushprivatedelivery.id = args.deliveryid;
				$scope.pushprivatedelivery.time = args.newtime;
				$scope.pushprivatedelivery.price = args.newprice;
				$scope.ActiveTab = 3;
			}
		}, 300);
		*/

	
		if ($scope.ActiveTab == 1 || $scope.ActiveTab == 4)
			$scope.GetDeliveries(1);
		
	});
	
	
	
	$scope.GetTime = function(date)
	{
		$scope.splitDate = date.split(" ");
		$scope.SplitTime = $scope.splitDate[1].split(":");
		$scope.newTime = $scope.SplitTime[0]+':'+$scope.SplitTime[1];
		return $scope.newTime;
	}
	
	$scope.deliveryStatus = function(index)
	{
		var deliverystatusText = '';
		
		if (index == 0)
			deliverystatusText = 'חדש';
		
		if (index == 1)
			deliverystatusText = 'התקבל';

		if (index == 2)
			deliverystatusText = 'בטיפול';

		if (index == 3)
			deliverystatusText = 'נמסר';		
		
		return deliverystatusText;
	}

	//CustumerType 0 = normal delivery , 1 = private custumer
	$scope.GetDeliveries = function(CustumerType)
	{
		
		$scope.DeliverysArray = [];
		
		$scope.deliveryparams = 
		{
			"user" : window.localStorage.userid,
			"type" : $scope.user_type,
			"private_custumer" : CustumerType,
			"pushid" : $rootScope.pushId
		}
		

		SendPostToServer($scope.deliveryparams,$rootScope.LaravelHost+'/GetDeliveries',function(data, success) 
		{					
			$scope.DeliverysArray = data;
			//$scope.unseenDeliveries = 0;

			for (var i = 0; i < $scope.DeliverysArray.length; i++) 
			{
				$scope.uncompleteRegularDeliveries = $scope.DeliverysArray[i].countRegularDeliveries;
				$scope.uncompletePrivateDeliveries = $scope.DeliverysArray[i].countPrivateDeliveries;
				$scope.unreadChatMessages = $scope.DeliverysArray[i].countUnreadChat;
				//$scope.DeliverysArray[i].timeupdated = 0;
			}
			
			$ionicScrollDelegate.scrollTop();
			console.log("DeliverysArray: ", data);
		});	
	}
	
	$scope.getPrivateDeliveryResponse = function()
	{
		
		$scope.sendparams = 
		{
			"user" : window.localStorage.userid,
			"pushid" : $rootScope.pushId
		}
		

		SendPostToServer($scope.sendparams,$rootScope.LaravelHost+'/getPrivateDeliveryResponse',function(data, success) 
		{	
			 $scope.privateDeliveryResponseArray = data;
			 
			 if (data.length > 0)
				 $scope.pushprivatedelivery.enabled = true;
			 
			 console.log("privateDeliveryResponseArray" , data)
			//$scope.pushprivatedelivery.enabled = true;
		});	
	}
	
	
	$scope.selectPrivateDeliveryman = function(index,id)
	{
		
		$scope.getDeliveryMan();
		$scope.deliveryfields.index = index;
		$scope.deliveryfields.id = id;
		
		
	   $ionicModal.fromTemplateUrl('templates/private_modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(SelectPrivatePopup) {
		  $scope.SelectPrivatePopup = SelectPrivatePopup;
		  $scope.SelectPrivatePopup.show();
		});
	}
	
	$scope.closeSelectPrivateDeliveryman = function()
	{
		$scope.SelectPrivatePopup.hide();
	}
	
	$scope.sendPrivateDeliveryman = function()
	{
		if ($scope.deliveryfields.deliveryman =="")
		{
			$ionicPopup.alert({
			 title: 'יש לבחור שליח',
			 template: ''
			});				
		}
		else
		{
			
			$scope.sendparams = 
			{
				"user" : window.localStorage.userid,
				"id" : $scope.deliveryfields.id,
				"recipent" : $scope.deliveryfields.deliveryman,
				"pushid" : $rootScope.pushId
			}
			

			SendPostToServer($scope.sendparams,$rootScope.LaravelHost+'/SendPrivateCustumerDelivery',function(data, success) 
			{	
				$scope.DeliverysArray[$scope.deliveryfields.index].private_delivery_status = 3;
				$scope.SelectPrivatePopup.hide();
			});	


			
		}
	}
	
	
	//type 0 = save,1 = update
	$scope.privateCustumerPopup = function(index,id,recipent,type)
	{

		
	   $scope.privateCustumerSaveType = type;
	   $scope.privatedelivery.index = index;
	   $scope.privatedelivery.id = id;
	   $scope.privatedelivery.recipent = recipent;
	

	   if (type == "1")
	   {
		   if ($scope.DeliverysArray[index].time)
			   $scope.privatedelivery.time = $scope.DeliverysArray[index].time;
		   
		   if ($scope.DeliverysArray[index].price)
			   $scope.privatedelivery.price = $scope.DeliverysArray[index].price;
	   }
	   
	   
	   $ionicModal.fromTemplateUrl('templates/private_custumerpopup.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(privatePopup) {
		  $scope.privatePopup = privatePopup;
		  $scope.privatePopup.show();
		});
	}
	
	$scope.closePrivatePopup = function()
	{
		$scope.privatePopup.hide();
	}
	
	$scope.confirmPrivateDelivery = function(index,id)
	{
		
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר הזמנה?',
		 template: '',
		 cancelText: "ביטול",
		 okText: "אישור"	 
	   });
    	confirmPopup.then(function(res) {
		if(res) 
		{
			
			$scope.params = 
			{
				"user" : window.localStorage.userid,
				"id" : id,
				"pushid" : $rootScope.pushId
			}
			
			SendPostToServer($scope.params,$rootScope.LaravelHost+'/ConfirmPrivateOrder',function(data, success) 
			{					
				//$scope.DeliverysArray[index].private_delivery_status = 2;
				$scope.pushprivatedelivery.enabled = false;
				$scope.pushprivatedelivery.id = '';
				$scope.pushprivatedelivery.time = '';
				$scope.pushprivatedelivery.price = '';	

				$ionicPopup.alert({
				 title: 'נשלח בהצלחה',
				 template: ''
				});					
			});	
		} 
	   });	


	   
	}
	
	$scope.cancelDelivery = function(index,id)
	{
		/* get delivery index */
		for (var i = 0; i < $scope.DeliverysArray.length; i++) 
		{
			if ($scope.DeliverysArray[i].index == id)
				$scope.deliveryIndex = i;
		}	
		
		

	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר ביטול משלוח?',
		 template: '',
		 cancelText: "ביטול",
		 okText: "אישור"	 
	   });
    	confirmPopup.then(function(res) {
		if(res) 
		{
			
			$scope.params = 
			{
				"user" : window.localStorage.userid,
				"id" : id,
				"pushid" : $rootScope.pushId
			}
			
			SendPostToServer($scope.params,$rootScope.LaravelHost+'/CancelDelivery',function(data, success) 
			{					
				$scope.DeliverysArray.splice($scope.deliveryIndex, 1);
				$scope.pushprivatedelivery.enabled = false;
				$scope.pushprivatedelivery.id = '';
				$scope.pushprivatedelivery.time = '';
				$scope.pushprivatedelivery.price = '';		
			});	
		} 
	   });	
	}
	
	$scope.savePrivateDelivery = function()
	{
		// get delivery index
		for (var i = 0; i < $scope.DeliverysArray.length; i++) 
		{
			if ($scope.DeliverysArray[i].index == $scope.privatedelivery.id)
				$scope.privatedelivery.index =  i;
		}	
		
		//$scope.privatedelivery.time = "18:00";
		//$scope.privatedelivery.price = "20";
			
		if ($scope.privatedelivery.time == "")
		{
			$ionicPopup.alert({
			 title: 'יש להזין שעת אספקה',
			 template: ''
			});				
		}
		
		else if ($scope.privatedelivery.price == "")
		{
			$ionicPopup.alert({
			 title: 'יש להזין מחיר',
			 template: ''
			});				
		}
		else
		{
			if ($scope.privateCustumerSaveType == 1)
				$scope.updateURL = 'UpdatePrivateDelivery';
			else
				$scope.updateURL = 'SendPrivateDelivery';
			

			SendPostToServer($scope.privatedelivery,$rootScope.LaravelHost+'/'+$scope.updateURL,function(data, success) 
			{			
				$scope.DeliverysArray[$scope.privatedelivery.index].private_delivery_status = 1;
				$scope.closePrivatePopup();
			});
		}
	}	

	
	$scope.updateDeliveryStatus = function(index,id,deliverystatus)
	{
		// get delivery index
		for (var i = 0; i < $scope.DeliverysArray.length; i++) 
		{
			if ($scope.DeliverysArray[i].index == id)
				$scope.deliveryIndex =  i;
		}	
		
		$scope.sendUpdate = 0;

		if (deliverystatus == 0) {
			$scope.DeliverysArray[$scope.deliveryIndex].status = 1;
			$scope.sendUpdate = 1;
			$scope.newStatus = 1;
		}
		
		if (deliverystatus == 1) {
			$scope.DeliverysArray[$scope.deliveryIndex].status = 3;
			$scope.sendUpdate = 1;
			$scope.newStatus = 3;
		}
		
		/*
		if (deliverystatus == 2) {
			$scope.DeliverysArray[$scope.deliveryIndex].status = 3;
			$scope.sendUpdate = 1;
			$scope.newStatus = 3;
		}
		*/
		
		
	    if ($scope.sendUpdate == 1)
		{
			$scope.sendparams = 
			{
				"user" : window.localStorage.userid,
				"id": id,
				"status" : $scope.newStatus,
				"pushid" : $rootScope.pushId
			}
			
			
			SendPostToServer($scope.sendparams,$rootScope.LaravelHost+'/UpdateDeliveryStatus',function(data, success) 
			{					
				
			});
		}
	}
	
	$scope.getDeliveryMan = function()
	{
		$scope.sendparams = 
		{
			"user" : window.localStorage.userid,
			"pushid" : $rootScope.pushId
		}
		

		SendPostToServer($scope.sendparams,$rootScope.LaravelHost+'/GetDeliveryman',function(data, success) 
		{					
			$scope.deliverymanJson = data;
		});
	}
	
	$scope.openTime = function()
	{
		var options = {
			date: new Date(),
			mode: 'time',
			//minuteInterval: 10,
			is24Hour : true
		};
		
		
		datePicker.show(options, function(date){
			$scope.newHour = date.toString().split(" ")[4];
			$scope.splitHour = $scope.newHour.split(":");
			$scope.newTime = $scope.splitHour[0]+":"+$scope.splitHour[1];
			$scope.deliveryfields.time = $scope.newTime;
			$scope.privatedelivery.time = $scope.newTime;
			$scope.updateTime();
			//alert ($scope.newTime)
		
		});
	}
	
	$scope.updateTime = function()
	{
		 $timeout(function() {
			$scope.deliveryfields.time = $scope.newTime;
		}, 100);
	}

	
	
	
	$scope.openTimePopup = function(index,id,ordertime,companyid,newtime,deliveryman,type)
	{
		$scope.openTimePopupType = String(type);
		//$scope.getDeliveryMan();
		$scope.deliveryfields.index = index;
		$scope.deliveryfields.id = id;
		$scope.deliveryfields.deliverytime = $scope.GetTime(ordertime);
		$scope.deliveryfields.company_id = companyid;
		$scope.deliveryfields.deliveryman = deliveryman;
		
		if (newtime && type == 0)
			$scope.deliveryfields.timeupdated = 1;
		else
			$scope.deliveryfields.timeupdated = 0;
		

	   $ionicModal.fromTemplateUrl('templates/time_modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(timeModal) {
		  $scope.timeModal = timeModal;
		  $scope.timeModal.show();
		});
	}
	
	$scope.openDeliveryPopup = function(index,id,companyid)
	{
		$scope.deliveryPopupType = 0;
		$scope.getDeliveryMan();
		$scope.deliveryfields.index = index;
		$scope.deliveryfields.id = id;
		$scope.deliveryfields.company_id = companyid;
		
		
	   $ionicModal.fromTemplateUrl('templates/deliveryman_select.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(deliverymanModal) {
		  $scope.deliverymanModal = deliverymanModal;
		  $scope.deliverymanModal.show();
		});		
	}
	
	
	$scope.changeDeliveryPopup = function(index,id,companyid,deliverymanid)
	{
		$scope.deliveryPopupType = 1;
		$scope.getDeliveryMan();
		$scope.deliveryfields.index = index;
		$scope.deliveryfields.id = id;
		$scope.deliveryfields.company_id = companyid;
		//$scope.deliveryfields.deliveryman = deliverymanid;
		$scope.deliveryfields.previousdeliveryman = deliverymanid;
		

	   $ionicModal.fromTemplateUrl('templates/deliveryman_select.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(deliverymanModal) {
		  $scope.deliverymanModal = deliverymanModal;
		  $scope.deliverymanModal.show();
		});	
		
	}
	
	$scope.closeDeliverymanModal = function()
	{
		$scope.deliverymanModal.hide();
	}
	
	
	$scope.showDeliveryInfo = function(item)
	{
		$scope.deliveryInfoArray = item;
		
	   $ionicModal.fromTemplateUrl('templates/delivery_info_popup.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(DeliveryPopUp) {
		  $scope.DeliveryPopUp = DeliveryPopUp;
		  $scope.DeliveryPopUp.show();
		});		
	}
	
	$scope.closeDeliveryInfoPopup = function()
	{
		$scope.DeliveryPopUp.hide();
	}
	
	
	$scope.changeDeliveryman = function()
	{
		if ($scope.deliveryfields.deliveryman =="")
		{
			$ionicPopup.alert({
			 title: 'יש לבחור שליח',
			 template: ''
			});	
		}
		else
		{

			//get new deliveryman name
			for (var i = 0; i < $scope.deliverymanJson.length; i++) 
			{
				if ($scope.deliverymanJson[i].index == $scope.deliveryfields.deliveryman)
					$scope.deliveryfields.newdeliverymanname = $scope.deliverymanJson[i].name;
			}

			// get delivery index
			for (var i = 0; i < $scope.DeliverysArray.length; i++) 
			{
				if ($scope.DeliverysArray[i].index == $scope.deliveryfields.id)
					$scope.deliveryfields.index = i;
			}	

			

			
			$scope.sendparams = 
			{
				"user" : window.localStorage.userid,
				"id" : $scope.deliveryfields.id,
				"previousdeliveryman" : $scope.deliveryfields.previousdeliveryman,			
				"deliveryman" : $scope.deliveryfields.deliveryman,
				"company_id" : $scope.deliveryfields.company_id,
				"pushid" : $rootScope.pushId
			}
			
			SendPostToServer($scope.sendparams,$rootScope.LaravelHost+'/ChangeDeliveryMan',function(data, success) 
			{	

					$ionicPopup.alert({
					 title: 'שליח הוחלף בהצלחה',
					 template: ''
					});	

				$scope.DeliverysArray[$scope.deliveryfields.index].deliverman_id = $scope.deliveryfields.deliveryman;
				$scope.DeliverysArray[$scope.deliveryfields.index].deliveryData[0].name = $scope.deliveryfields.newdeliverymanname;
				$scope.closeDeliverymanModal();
			});	
		}
	}
	
	
	$scope.updateDeliveryman = function()
	{
		if ($scope.deliveryfields.deliveryman == "")
		{
			$ionicPopup.alert({
			 title: 'יש לבחור שליח',
			 template: ''
			});				
		}
		else
		{

	
			//get new deliveryman name
			for (var i = 0; i < $scope.deliverymanJson.length; i++) 
			{
				if ($scope.deliverymanJson[i].index == $scope.deliveryfields.deliveryman)
					$scope.deliveryfields.newdeliverymanname = $scope.deliverymanJson[i].name;
			}

			// get delivery index
			for (var i = 0; i < $scope.DeliverysArray.length; i++) 
			{
				if ($scope.DeliverysArray[i].index == $scope.deliveryfields.id)
					$scope.deliveryfields.index = i;
			}	



			$scope.sendparams = 
			{
				"user" : window.localStorage.userid,
				"id" : $scope.deliveryfields.id,
				"deliveryman" : $scope.deliveryfields.deliveryman,
				"company_id" : $scope.deliveryfields.company_id,
				"pushid" : $rootScope.pushId
			}
			
			
			SendPostToServer($scope.sendparams,$rootScope.LaravelHost+'/UpdateDeliveryMan',function(data, success) 
			{	
				$ionicPopup.alert({
				 title: 'הזמנה נשלחה לשליח',
				 template: ''
				});	

				//$scope.DeliverysArray[$scope.deliveryfields.index].status = 1;
				$scope.DeliverysArray[$scope.deliveryfields.index].deliverman_id = $scope.deliveryfields.deliveryman;
				//

				if ($scope.DeliverysArray[$scope.deliveryfields.index].deliveryData.length == 0)
				{
					$scope.DeliverysArray[$scope.deliveryfields.index].deliveryData.push({
						"name" : $scope.deliveryfields.newdeliverymanname,
						"index" : $scope.deliveryfields.deliveryman,
					});						
				}
				else
					$scope.DeliverysArray[$scope.deliveryfields.index].deliveryData[0].name = $scope.deliveryfields.newdeliverymanname;
					
				
				$scope.closeDeliverymanModal();
			});	
			//
		}
	}
	
	
	$scope.sendDelivery = function()
	{
		if ($scope.orderfields.details =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין פרטי ההזמנה',
			 template: ''
			});				
		}
		else
		{
			$scope.sendparams = 
			{
				"user" : window.localStorage.userid,
				"details" : $scope.orderfields.details,
				"desc" : $scope.orderfields.desc,
				"pushid" : $rootScope.pushId
			}
			

			SendPostToServer($scope.sendparams,$rootScope.LaravelHost+'/AddPrivateDelivery',function(data, success) 
			{	
				$ionicPopup.alert({
				 title: 'הזמנה התקבלה, תינתן תשובה עם מחיר וזמני אספקה בעוד מספר דקות , בברכה צוות אוורסט',
				 template: ''
				});		
			
				$scope.orderfields.details = '';
				$scope.orderfields.desc = '';
				//$scope.ActiveTab = 1;
			});			
		}

	}
	
	$scope.closeTimeModal = function()
	{
		 $scope.timeModal.hide();
	}
	
	

	$scope.saveDeliveryTime = function()
	{
	
		//$scope.deliveryfields.time = "16:00";
		//alert ($scope.deliveryfields.deliveryman);
	
		if ($scope.deliveryfields.time == "")
		{
			$ionicPopup.alert({
			 title: 'יש להזין זמן משלוח',
			 template: ''
			});				
		}
		
		
		/*
		else if($scope.deliveryfields.deliveryman == "")
		{
			$ionicPopup.alert({
			 title: 'יש לבחור שליח',
			 template: ''
			});				
		}
		*/
		
		else
		{
			
			// get delivery index
			for (var i = 0; i < $scope.DeliverysArray.length; i++) 
			{
				if ($scope.DeliverysArray[i].index == $scope.deliveryfields.id)
					$scope.deliveryfields.index = i;
			}	


			$scope.sendparams = 
			{
				"user" : window.localStorage.userid,
				"id" : $scope.deliveryfields.id,
				//"index" : $scope.deliveryfields.index,
				"time" : $scope.deliveryfields.time,
				"deliveryman" : $scope.deliveryfields.deliveryman,
				"deliverytime" : $scope.deliveryfields.deliverytime,
				"company_id" : $scope.deliveryfields.company_id,
				"timeupdated" : $scope.deliveryfields.timeupdated,
				"update_type" : $scope.openTimePopupType,
				"pushid" : $rootScope.pushId
			}
			


			SendPostToServer($scope.sendparams,$rootScope.LaravelHost+'/UpdateDeliveryTime',function(data, success) 
			{	
				if ($scope.openTimePopupType == "0") {
					$scope.DeliverysArray[$scope.deliveryfields.index ].time = $scope.deliveryfields.time;
				}
				//$scope.DeliverysArray[$scope.deliveryfields.index ].status = 0; 
				$scope.DeliverysArray[$scope.deliveryfields.index ].estimated_time = data.estimated_time;
				$scope.DeliverysArray[$scope.deliveryfields.index ].timeupdated =  $scope.deliveryfields.timeupdated;
				$scope.DeliverysArray[$scope.deliveryfields.index ].arrival_time_updated = $scope.openTimePopupType;

				$scope.closeTimeModal();
				$scope.deliveryfields.time = '';
				$scope.deliveryfields.deliveryman = '';
			});
		}
	}
	


	
	$scope.getCityBlur = function()
	{

	  $timeout(function() 
	  {

		if ($scope.fields.address.formatted_address)
		{
			console.log("address: " , $scope.fields.address);
			$scope.fields.city = $scope.fields.address.address_components[2].long_name;
			$scope.fields.street = $scope.fields.address.name; //$scope.fields.address.address_components[1].long_name;
			$scope.fields.housenumber = parseInt($scope.fields.address.address_components[0].long_name);	
		}
	  }, 300);			

	}
	
	
	$scope.checkPhone = function()
	{
	  $timeout(function() 
	  {
		 if ($scope.fields.phone)
		 {
			$scope.sendparams = 
			{
				"user" : window.localStorage.userid,
				"phone": $scope.fields.phone,
				"pushid" : $rootScope.pushId
			}

			SendPostToServer($scope.sendparams,$rootScope.LaravelHost+'/CheckExistingPhone',function(data, success) 
			{	
				if (data.status == 0)
					$scope.checkPhoneExists = 0;
				else
				{
					$scope.checkPhoneExists = 1;
					$scope.fields.address = data.address;
					//$scope.fields.address.formatted_address = data.address;
					$scope.fields.city = data.city;
					$scope.fields.street = data.street;
					$scope.fields.streetnumber = data.streetnumber;
					$scope.fields.housenumber = parseInt(data.housenumber);
					$scope.fields.location_lat = data.location_lat;
					$scope.fields.location_lng = data.location_lng;
				}
			});	
		 }

	  }, 300);			
	}
	


	$scope.$watch('fields.address', function (newValue, oldValue, scope) 
	{
		if ($scope.fields.address)
		{
			if ($scope.fields.address.formatted_address =="" || $scope.fields.address.formatted_address == undefined )
			{
				$scope.googlePlacesFound = true;
			}
			else
				$scope.googlePlacesFound = false;
		}
		else
		{
			$scope.googlePlacesFound = false;
		}

	},true);	
	
	$scope.addDelivery = function()
	{
		
		
		if ($scope.fields.phone =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין טלפון',
			 template: ''
			});	
		}

		
		else if ($scope.fields.address =="" && $scope.checkPhoneExists == 0)
		{
			$ionicPopup.alert({
			 title: 'יש להזין כתובת למשלוח',
			 template: ''
			});	
		}
		
		/*
		else if ($scope.fields.address.formatted_address =="" || $scope.fields.address.formatted_address == undefined && $scope.checkPhoneExists == 0)
		{

		}
		*/	

		
		/*
		else if ($scope.fields.city  =="" && $scope.checkPhoneExists == 0)
		{
			$ionicPopup.alert({
			 title: 'יש להזין עיר למשלוח',
			 template: ''
			});	
		}
		
		else if ($scope.fields.street  ==""&& $scope.checkPhoneExists == 0 )
		{
			$ionicPopup.alert({
			 title: 'יש להזין רחוב למשלוח',
			 template: ''
			});	
		}

		else if (isNaN($scope.fields.streetnumber) || $scope.fields.streetnumber == null && $scope.checkPhoneExists == 0)
		{
			$ionicPopup.alert({
			 title: 'יש להזין מספר רחוב',
			 template: ''
			});	
		}
		*/


		else
		{
			

			if ($scope.fields.address.formatted_address)
			{
				$scope.newaddressgeo = String($scope.fields.address.geometry.location);
				$scope.splitaddress = $scope.newaddressgeo.split(",");
				$scope.fields.location_lat = $scope.splitaddress[0].replace("(", "");
				$scope.fields.location_lng = $scope.splitaddress[1].replace(")", "");	
				$scope.fulladdress = $scope.fields.address.formatted_address;
	
			}
			else
				$scope.fulladdress = $scope.fields.address;

			
			$scope.sendparams = 
			{
				"user" : window.localStorage.userid,
				"address": $scope.fulladdress,//$scope.fields.address.formatted_address,
				"city": $scope.fields.city,
				"street": $scope.fields.street,
				"streetnumber": $scope.fields.streetnumber,
				"housenumber": $scope.fields.housenumber,
				"phone": $scope.fields.phone,
				"location_lat" : $scope.fields.location_lat,
				"location_lng" : $scope.fields.location_lng,
				"pushid" : $rootScope.pushId
			}
			
			
			SendPostToServer($scope.sendparams,$rootScope.LaravelHost+'/AddNewDelivery',function(data, success) 
			{					
				$scope.setActiveTab(1);
				$scope.fields.address = '';
				$scope.fields.city = '';
				$scope.fields.street = '';
				$scope.fields.streetnumber = '';
				$scope.fields.housenumber = '';
				$scope.fields.location_lat = '';
				$scope.fields.location_lng = '';
				$scope.fields.phone = '';
				$scope.googlePlacesFound = false;
			});	
		}
	}
	
	
	$scope.dialPhone = function(phone)
	{
		if (phone)
			window.open('tel:' + phone, '_system');
	}
	
	$scope.showInformationPopup = function()
	{
	   $ionicModal.fromTemplateUrl('templates/information_popup.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(informationPopup) {
		  $scope.informationPopup = informationPopup;
		  $scope.informationPopup.show();
		});
	}
	
	$scope.closeInformationPopup = function()
	{
		$scope.informationPopup.hide();
	}

	
	$scope.navigateLocation = function(lat,lng)
	{
		if (lat)
		{
          $timeout(function() 
          {
			$scope.wazelocation = lat+','+lng;
            window.location = "waze://?q="+$scope.wazelocation+"&navigate=yes";
          }, 300);			
		}
	}

	/* chat */
	
	$scope.splitDate = function(value)
	{
		var splitLastSeen = '';

		splitLastSeen =  value.split(" ");

		var date = splitLastSeen[0];
		var hour = splitLastSeen[1];
	
		var splitdate =  date.split("-");
		var splithour =  hour.split(":");
	
		return ''+splitdate[2]+'/'+splitdate[1]+'/'+splitdate[0]+' '+ splithour[0]+':'+splithour[1]+'';		
	}	
	
	
	$rootScope.$on('newchatmsg', function(event, args) 
	{
	  
	  $timeout(function() 
	  {
			if ($scope.user_type !="2" && $scope.ActiveTab != 2 )
			{
				if (args.userid != $scope.user_local_storage)
				{
					$scope.unreadChatMessages++;
				}				
			}

		  
		 //update/refresh messages for admin
		 if ($scope.user_type == 2)
		 {
			 if (args.updateadmin == 1)
			 {
				 $scope.getAdminMessages();
			 }			 
		 }
		 
		if (args.userid == 1)
		{
			//alert(JSON.stringify(args));
			  
			$scope.date = new Date()
			$scope.hours = $scope.date.getHours()
			$scope.minutes = $scope.date.getMinutes()
			$scope.seconds = $scope.date.getSeconds()
		
			if ($scope.hours < 10)
			$scope.hours = "0" + $scope.hours
			
			if ($scope.minutes < 10)
			$scope.minutes = "0" + $scope.minutes
			$scope.time = $scope.hours+':'+$scope.minutes;


			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			
			if(dd<10) {
				dd='0'+dd
			} 

			if(mm<10) {
				mm='0'+mm
			} 

			$scope.today = dd+'/'+mm+'/'+yyyy;
			$scope.newdate = $scope.today+' '+$scope.time;

			
			$scope.chat  = 
			{
				"text" : args.text,
				"userimage" : args.userimage,
				"userid" : args.userid,
				"time" : $scope.time,
				"date" : $scope.newdate,
				"isImage" : args.isImage,
			}
			
			
			
			if ($scope.chatArray.length == 0)
				$scope.chatArray = new Array();		
			
						
			$scope.chatArray.push($scope.chat);	
			
			if ($scope.user_type !="2")
				$ionicScrollDelegate.scrollBottom();		
		}

		


	  }, 300);		
	
			
	});


	$rootScope.$on('movetab', function(event, args) 
	{
	  
	  $timeout(function() 
	  {
		  if (args)
		  {
			  $scope.setActiveTab(args);
		  }
	  }, 300);		
	
			
	});
	
	
	
	
	$scope.getChatHistory = function()
	{

		$scope.sendfields = 
		{
			"user" : window.localStorage.userid,
			"serverhost" : $scope.host,
			"pushid" : $rootScope.pushId
		}
		SendPostToServer($scope.sendfields,$rootScope.LaravelHost+'/GetChatHistory',function(data, success) 
		{
			console.log("chat history: " , data);
			
			
			if (data.length == 0)
				$scope.chatArray = new Array();
			else
			{
				$scope.chatArray = data;
				//$scope.chatArray.reverse();
			}
				
			
			$ionicScrollDelegate.scrollBottom();					
		});		
	
	}
	
	$scope.getAdminMessages = function()
	{

		$scope.countUnreadResturants = 0;	
		$scope.countUnreadDeliveryman = 0;
		$scope.countUnreadPrivateCustumer = 0;
		
		$scope.sendfields = 
		{
			"user" : window.localStorage.userid,
			"pushid" : $rootScope.pushId
		}
		SendPostToServer($scope.sendfields,$rootScope.LaravelHost+'/getAdminMessages',function(data, success) 
		{
			console.log("getAdminMessages: " , data);
			$scope.AdminMessagesArray = data;
			$ionicScrollDelegate.scrollTop();
			//$scope.messagesArray = angular.copy(data);
				
			//count unread messages for tabs.
			for (var i = 0; i < data.length; i++) 
			{

				if (data[i].is_read != "0")
				{
					//console.log("type", $scope.messagesArray[i].info[0].type)
					//resturants
					if (data[i].info[0].type == "0")
						$scope.countUnreadResturants++;
						

					//deliveryman
					 if (data[i].info[0].type == "1")
						 $scope.countUnreadDeliveryman++;
						
					
					//private custumer
					 if (data[i].info[0].type == "3")
						 $scope.countUnreadPrivateCustumer++;					
				}
			}
						
		});			
	}
	
	
	
	$scope.sendChat = function(isImage,fileName)
	{
		
			if (isImage == 0)
				$scope.textField = $scope.fields.chatbox;
			else
				$scope.textField = fileName;

			
		if ($scope.textField)
		{
			
			$scope.date = new Date()
			$scope.hours = $scope.date.getHours()
			$scope.minutes = $scope.date.getMinutes()
			$scope.seconds = $scope.date.getSeconds()
		
			if ($scope.hours < 10)
			$scope.hours = "0" + $scope.hours
			
			if ($scope.minutes < 10)
			$scope.minutes = "0" + $scope.minutes
			$scope.time = $scope.hours+':'+$scope.minutes;


			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			
			if(dd<10) {
				dd='0'+dd
			} 

			if(mm<10) {
				mm='0'+mm
			} 

			$scope.today = dd+'/'+mm+'/'+yyyy;
			$scope.newdate = $scope.today+' '+$scope.time;


			

			$scope.sendfields = 
			{
				"user" : window.localStorage.userid,
				"recipent" : 1,//window.localStorage.userid , //everest manager
				"text" : $scope.textField,
				"time" : $scope.time,
				"serverhost" : $scope.host,		
				"isImage" : isImage,
				"sendToAdmin" : "1",
				"pushid" : $rootScope.pushId
				
			}
			SendPostToServer($scope.sendfields,$rootScope.LaravelHost+'/SendNewMessage',function(data, success) 
			{
				
				$scope.chat  = 
				{
					"username" : window.localStorage.name ,
					"text" : $scope.textField,
					"userimage" : window.localStorage.image,
					"userid" : window.localStorage.userid,
					"time" : $scope.time,
					"date" : $scope.newdate,
					"isImage" : isImage	
				}

				console.log("chatArray: ", $scope.chatArray)
				
				if ($scope.chatArray.length == 0)
					$scope.chatArray = new Array();
					

				$scope.chatArray.push($scope.chat);
				$scope.fields.chatbox = '';
				$ionicScrollDelegate.scrollBottom();					
			});	
		}
	}
	
	
	
	$scope.enterPress = function(keyEvent)
	{
		if (keyEvent.which === 13)
		{
			$scope.sendChat(0);
		}
	}

    $scope.showChatModal = function(newimage)
    {
        $scope.bigChatImage = newimage;
        
        $ionicModal.fromTemplateUrl('chat-image-modal.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(chatImageModal) {
          $scope.chatImageModal = chatImageModal;
          $scope.chatImageModal.show();
         });        
    }


    $scope.closeChatImage = function()
    {
        $scope.bigChatImage = '';
        $scope.chatImageModal.hide();
    }
	
	

	$scope.imageOptions = function()
	{

			var myPopup = $ionicPopup.show({
			//template: '<input type="text" ng-model="data.myData">',
			//template: '<style>.popup { width:500px; }</style>',
			title: 'בחירת מקור התמונה:',
			scope: $scope,
			cssClass: 'custom-popup',
			buttons: [


		   {
			text: 'מצלמה',
			type: 'button-positive',
			onTap: function(e) { 
			  $scope.takePicture(1);
			}
		   },
		   {
			text: 'גלריית תמונות',
			type: 'button-calm',
			onTap: function(e) { 
			 $scope.takePicture(0);
			}
		   },
		   
			{
			text: 'ביטול',
			type: 'button-assertive',
			onTap: function(e) {  
			  //alert (1)
			}
		   },
		   
		   ]
		  });
		  
		  //ClosePopupService.register(myPopup);
	
	}
	
	


	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.chunkedMode = false;
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			//UploadImg1.php
			ft.upload($scope.imgURI, encodeURI($rootScope.LaravelHost+'/uploadImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			//console.log(data)
			 $timeout(function() {

				if (data.response)
				{
					
					$scope.sendChat(1,data.response);

				}
				
				}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			//alert(JSON.stringify(data));
			alert("onUploadFail : " + data);
		}
    }	
	
	


	
	
	
	$scope.enterPress = function(keyEvent)
	{
		if (keyEvent.which === 13)
		{
			$scope.sendChat(0);
		}
	}	
	
	/* end chat functions */
	
	/* start background location */
	
	
	watchID = null;
	
	if (!window.localStorage.backgroundLocation)
		window.localStorage.backgroundLocation = "0";
	
	if (!window.localStorage.locationWatchId)
		window.localStorage.locationWatchId = null;
	
	
	$scope.BackgroundLocation = window.localStorage.backgroundLocation;
	
	$scope.resumeLocation = function()
	{
		//if ($scope.BackgroundLocation == "1")
			if (window.localStorage.backgroundLocation == "1")
			$scope.getUserLocation();
	}
	
	
	$scope.getUserLocation = function(mode)
	{
		
		//CheckGPS.check(function(){
			
		var posOptions = {enableHighAccuracy: false};

		$cordovaGeolocation
			.getCurrentPosition(posOptions)
			.then(function (position) {
				$ionicLoading.hide();	
				$scope.updateUserLocation(position.coords.latitude,position.coords.longitude)
				$scope.BackgroundLocation = "1";
				$scope.toggleUserLocation($scope.BackgroundLocation);			
				window.localStorage.backgroundLocation = $scope.BackgroundLocation;
			}, function (err) {
				$ionicLoading.hide();	
				$scope.BackgroundLocation = "0";
				$scope.toggleUserLocation($scope.BackgroundLocation);
				window.localStorage.backgroundLocation = $scope.BackgroundLocation;				
				
			var alertPopup = $ionicPopup.alert({
			  title: 'יש לאפשר שירותי מיקום מעביר להגדרות',
			  template: ''
			});
			alertPopup.then(function(res) {
				
				 if (ionic.Platform.isIOS() == true)
					 cordova.plugins.diagnostic.switchToSettings();
				 else
					 cordova.plugins.diagnostic.switchToLocationSettings();
			});	

			
			});


		//  },
		  //function(){
			
			
			
		 // });				
	}
	
	$scope.toggleUserLocation = function(mode)
	{
		$scope.sendfields = 
		{
			"user" : window.localStorage.userid,
			"mode" : mode,
		}
		SendPostToServer($scope.sendfields,$rootScope.LaravelHost+'/toggleUserLocation',function(data, success) 
		{
			$scope.BackgroundLocation = String(mode);
			window.localStorage.backgroundLocation = String(mode);		
		});	
	}

			
	$scope.enableBackGroundLocation = function()
	{
		
        $ionicLoading.show({
          template: '<ion-spinner icon="lines" class="spinner-assertive"></ion-spinner>',
          noBackdrop : true,
          duration : 10000
        });
		
		
		if ($scope.BackgroundLocation == "1")
		{
			$ionicLoading.hide();	
			$scope.BackgroundLocation = "0";
			$scope.toggleUserLocation($scope.BackgroundLocation);		
		}
			
		else
		{	
			$ionicLoading.hide();	
			$scope.BackgroundLocation = "1";
			$scope.getUserLocation($scope.BackgroundLocation);
			/*
			if (window.cordova)
			{
				$scope.getUserLocation($scope.BackgroundLocation);
			}
			else
			{
				$ionicLoading.hide();	
				$scope.BackgroundLocation = "1";
				$scope.toggleUserLocation($scope.BackgroundLocation);		
			}
			*/
		}
		
		
		
		window.localStorage.backgroundLocation = $scope.BackgroundLocation;
	}
	
	$scope.$watch('BackgroundLocation', function () 
	{   
		if (window.localStorage.backgroundLocation == "1")
		//if ($scope.BackgroundLocation == "1")
		{
			
			var IntervalLocation = setInterval(function()
			{
			  $scope.trackUserLocation();	
			}, 20000);
			
			window.localStorage.locationWatchId = IntervalLocation;

		  
		}
			
		else
		{
			if (window.localStorage.locationWatchId != null) {
				//navigator.geolocation.clearWatch(window.localStorage.locationWatchId);
				clearInterval(window.localStorage.locationWatchId);
				window.localStorage.locationWatchId = null;
			}

			//navigator.geolocation.clearWatch(window.localStorage.locationWatchId);
	
		}
	});

	$scope.trackUserLocation = function()
	{
		function onSuccess(position) {
			$scope.updateUserLocation(position.coords.latitude,position.coords.longitude)
			//alert (position.coords.latitude);
			//alert (position.coords.longitude)
		}

		// onError Callback receives a PositionError object
		//
		function onError(error) {
			//alert('code: '    + error.code    + '\n' +
			//	  'message: ' + error.message + '\n');
		}

		options = {
		  enableHighAccuracy: false,
		  timeout: 10000,
		  maximumAge: 0
		};

		navigator.geolocation.getCurrentPosition(onSuccess, onError, options);

		//var watchID = navigator.geolocation.watchPosition(onSuccess, onError, options);
		//window.localStorage.locationWatchId = watchID;
	}
	
	$scope.updateUserLocation = function(lat,lng)
	{
		if (window.localStorage.backgroundLocation == "1")
		{
			$scope.sendfields = 
			{
				"user" : window.localStorage.userid,
				"location_lat" : lat,
				"location_lng" : lng,
			}
			SendPostToServer($scope.sendfields,$rootScope.LaravelHost+'/UpdateUserLocation',function(data, success) 
			{
						
			});				
		}
	}
	
	
	/* end background location */
	
	document.addEventListener("resume", resumefun, false);
	
	function resumefun()
	{
       if ($rootScope.State == "app.companymain")
	   {
		   
			if ($scope.ActiveTab == 1)
				$scope.GetDeliveries(0);

			if ($scope.ActiveTab == 4 && $scope.user_type == 2)
				$scope.GetDeliveries(1);
		   
			if ($scope.ActiveTab == 2 && $scope.user_type != 2)
				$scope.getChatHistory();
			
			if ($scope.ActiveTab == 2 && $scope.user_type == 2)
				$scope.getAdminMessages();
			
			if ($scope.ActiveTab == 3 && $scope.user_type == 3)
				$scope.getPrivateDeliveryResponse();	

			if ($scope.user_type == 1)
				$scope.resumeLocation();

			
	   }
	}
	
	/*
    $ionicPlatform.on("resume", function(event) {
		
    });
	*/
	

})


//admin chat controller.
.controller('ChatCtrl', function($scope, $stateParams,$ionicPopup,$localStorage,$rootScope,SendPostToServer,$ionicScrollDelegate,$timeout,$ionicModal,$ionicPlatform,$cordovaCamera) {

	$scope.recipentId = $stateParams.ItemId;
	$scope.navTitle = "";
	$scope.host = $rootScope.serverHost;
	$scope.chatArray = new Array();
	$scope.user_local_storage = window.localStorage.userid;
	$scope.user_type = window.localStorage.usertype ;	
	$rootScope.previousTab = 2;
	$rootScope.showLoading = 1;


	$scope.fields = 
	{
		"chatbox" : ""
	}
	
	$scope.splitDate = function(value)
	{
		var splitLastSeen = '';

		splitLastSeen =  value.split(" ");

		var date = splitLastSeen[0];
		var hour = splitLastSeen[1];
	
		var splitdate =  date.split("-");
		var splithour =  hour.split(":");
	
		return ''+splitdate[2]+'/'+splitdate[1]+'/'+splitdate[0]+' '+ splithour[0]+':'+splithour[1]+'';		
	}


	$rootScope.$on('newadminchatmsg', function(event, args) 
	{
		if (args.userid == $scope.recipentId)
		{

		  $timeout(function() 
		  {
			  
			//$scope.unreadChatMessages++;
			 
			  
			$scope.date = new Date()
			$scope.hours = $scope.date.getHours()
			$scope.minutes = $scope.date.getMinutes()
			$scope.seconds = $scope.date.getSeconds()
		
			if ($scope.hours < 10)
			$scope.hours = "0" + $scope.hours
			
			if ($scope.minutes < 10)
			$scope.minutes = "0" + $scope.minutes
			$scope.time = $scope.hours+':'+$scope.minutes;


			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			
			if(dd<10) {
				dd='0'+dd
			} 

			if(mm<10) {
				mm='0'+mm
			} 

			$scope.today = dd+'/'+mm+'/'+yyyy;
			$scope.newdate = $scope.today+' '+$scope.time;	
			
			$scope.chat  = 
			{
				"text" : args.text,
				"userimage" : args.userimage,
				"userid" : args.userid,
				"time" : $scope.time,
				"date" : $scope.newdate,
				"isImage" : args.isImage
			}
			
			
			
			if ($scope.chatArray.length == 0)
				$scope.chatArray = new Array();		
			
						
			$scope.chatArray.push($scope.chat);
			$ionicScrollDelegate.scrollBottom();


		  }, 300);				
		}
	
	
			
	});



	
	
	$scope.GetAdminChatHistory = function()
	{

		$scope.sendfields = 
		{
			"user" : window.localStorage.userid,
			"serverhost" : $scope.host,
			"recipent" : $scope.recipentId,
			"pushid" : $rootScope.pushId
		}
		SendPostToServer($scope.sendfields,$rootScope.LaravelHost+'/GetAdminChatHistory',function(data, success) 
		{
			console.log("chat history: " , data);
			
			
			if (data.length == 0)
				$scope.chatArray = new Array();
			else
			{
				$scope.chatArray = data;
				//$scope.chatArray.reverse();
			}
			
			$ionicScrollDelegate.scrollBottom();					
		});		
	
	}
	
	
	$scope.GetAdminChatHistory();
	

	$scope.sendChat = function(isImage,fileName)
	{
		
			if (isImage == 0)
				$scope.textField = $scope.fields.chatbox;
			else
				$scope.textField = fileName;

			
		if ($scope.textField)
		{
			
			$scope.date = new Date()
			$scope.hours = $scope.date.getHours()
			$scope.minutes = $scope.date.getMinutes()
			$scope.seconds = $scope.date.getSeconds()
		
			if ($scope.hours < 10)
			$scope.hours = "0" + $scope.hours
			
			if ($scope.minutes < 10)
			$scope.minutes = "0" + $scope.minutes
			$scope.time = $scope.hours+':'+$scope.minutes;


			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			
			if(dd<10) {
				dd='0'+dd
			} 

			if(mm<10) {
				mm='0'+mm
			} 

			$scope.today = dd+'/'+mm+'/'+yyyy;
			$scope.newdate = $scope.today+' '+$scope.time;

			

			$scope.sendfields = 
			{
				"user" : window.localStorage.userid,
				"recipent" : $scope.recipentId,
				"text" : $scope.textField,
				"time" : $scope.time,
				//"date" : $scope.newdate,
				"serverhost" : $scope.host,		
				"isImage" : isImage,
				"sendToAdmin" : "0",
				"pushid" : $rootScope.pushId				
				
			}
			SendPostToServer($scope.sendfields,$rootScope.LaravelHost+'/SendNewMessage',function(data, success) 
			{
				
				$scope.chat  = 
				{
					"username" : window.localStorage.name ,
					"text" : $scope.textField,
					"userimage" : window.localStorage.image,
					"userid" : window.localStorage.userid,
					"recipent" : $scope.recipentId,
					"time" : $scope.time,
					"date" : $scope.newdate,
					"isImage" : isImage	
				}

				console.log("chatArray: ", $scope.chatArray)
				
				if ($scope.chatArray.length == 0)
					$scope.chatArray = new Array();
					

				$scope.chatArray.push($scope.chat);
				$scope.fields.chatbox = '';
				$ionicScrollDelegate.scrollBottom();					
			});	
		}
	}
	
	
	
	$scope.enterPress = function(keyEvent)
	{
		if (keyEvent.which === 13)
		{
			$scope.sendChat(0);
		}
	}



	$scope.imageOptions = function()
	{

			var myPopup = $ionicPopup.show({
			//template: '<input type="text" ng-model="data.myData">',
			//template: '<style>.popup { width:500px; }</style>',
			title: 'בחירת מקור התמונה:',
			scope: $scope,
			cssClass: 'custom-popup',
			buttons: [


		   {
			text: 'מצלמה',
			type: 'button-positive',
			onTap: function(e) { 
			  $scope.takePicture(1);
			}
		   },
		   {
			text: 'גלריית תמונות',
			type: 'button-calm',
			onTap: function(e) { 
			 $scope.takePicture(0);
			}
		   },
		   
			{
			text: 'ביטול',
			type: 'button-assertive',
			onTap: function(e) {  
			  //alert (1)
			}
		   },
		   
		   ]
		  });
		  
		  //ClosePopupService.register(myPopup);
	
	}
	
	


	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.chunkedMode = false;
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			//UploadImg1.php
			ft.upload($scope.imgURI, encodeURI($rootScope.LaravelHost+'/uploadImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			//console.log(data)
			 $timeout(function() {

				if (data.response)
				{
					
					$scope.sendChat(1,data.response);

				}
				
				}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			//alert(JSON.stringify(data));
			alert("onUploadFail : " + data);
		}
    }	
	
	
	
    $scope.showChatModal = function(newimage)
    {
        $scope.bigChatImage = newimage;
        
        $ionicModal.fromTemplateUrl('chat-image-modal.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(chatImageModal) {
          $scope.chatImageModal = chatImageModal;
          $scope.chatImageModal.show();
         });        
    }


    $scope.closeChatImage = function()
    {
        $scope.bigChatImage = '';
        $scope.chatImageModal.hide();
    }


	document.addEventListener("resume", resumechatfun, false);

	function resumechatfun()
	{
		if ($rootScope.State == "app.chat")
		   $scope.GetAdminChatHistory();		
	}
	
	/*
    $ionicPlatform.on("resume", function(event) {

    });
	*/

})

.controller('UpdateInfoCtrl', function($scope, $stateParams,$ionicPopup,$localStorage,$rootScope,SendPostToServer,$ionicScrollDelegate,$timeout,$ionicModal,$ionicPlatform,$cordovaCamera) {

	$rootScope.showLoading = 1;
	
	
	$scope.fields = 
	{
		"name" : window.localStorage.name ,
		"city" : window.localStorage.city,
		"address" : window.localStorage.address,
		"house_number" : window.localStorage.house_number,
		"floor" : window.localStorage.floor,
		"apartment" : window.localStorage.apartment,
		"phone" : window.localStorage.phone,
		"email" : window.localStorage.email	
	}
	
	$scope.saveInfo = function()
	{
		var emailRegex = /\S+@\S+\.\S+/;
		
		if ($scope.fields.name =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין שם לקוח',
			 template: ''
			});					
		}
		else if ($scope.fields.city =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין עיר',
			 template: ''
			});					
		}
		else if ($scope.fields.address =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין כתובת',
			 template: ''
			});					
		}
		else if ($scope.fields.house_number =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין מספר בית',
			 template: ''
			});					
		}
		else if ($scope.fields.floor =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין קומה',
			 template: ''
			});					
		}
		else if ($scope.fields.apartment =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין דירה',
			 template: ''
			});					
		}
		else if ($scope.fields.phone =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין טלפון',
			 template: ''
			});					
		}


		else if ($scope.fields.email !="" && emailRegex.test($scope.fields.email) == false)
		{

			$ionicPopup.alert({
			title: 'דוא"ל לא תקין יש לתקן',
			template: '',		
			buttons: [{
			text: 'אישור',
			type: 'button-positive',
			  }]
		   });	

		   $scope.fields.email =  '';
		}

		
		else
		{
			
			$scope.sendfields = 
			{
				"user" : window.localStorage.userid,
				"name" : $scope.fields.name,
				"city" : $scope.fields.city,
				"address" : $scope.fields.address,
				"house_number" : $scope.fields.house_number,
				"floor" : $scope.fields.floor,
				"apartment" : $scope.fields.apartment,
				"phone" : $scope.fields.phone,
				"email" : $scope.fields.email,
				"pushid" : $rootScope.pushId
				
			}
			SendPostToServer($scope.sendfields,$rootScope.LaravelHost+'/UpdatePersonalInfo',function(data, success) 
			{
				$ionicPopup.alert({
				title: 'עודכן בהצלחה',
				template: '',		
				buttons: [{
				text: 'אישור',
				type: 'button-positive',
				  }]
			   });
				window.localStorage.name  = $scope.fields.name;
				window.localStorage.city = $scope.fields.city;
				window.localStorage.address = $scope.fields.address;
				window.localStorage.house_number = $scope.fields.house_number;
				window.localStorage.floor = $scope.fields.floor;
				window.localStorage.apartment = $scope.fields.apartment;
				window.localStorage.phone = $scope.fields.phone;
				window.localStorage.email = $scope.fields.email;
				
				window.location ="#/app/companymain";					
			});	
		}
		
	}

					
})

.controller('MapAdminCtrl', function($scope, $stateParams,$ionicPopup,$localStorage,$rootScope,SendPostToServer,$ionicHistory,$state,$ionicPlatform,$http,$timeout,$interval) {
	  $scope.serverHost = $rootScope.serverHost;
      $scope.GoogleMapStyle = [];
	  $rootScope.mapCtrl = 1;
	  $rootScope.showLoading = 0;
	  
		var myLatlng = '';
		var mapOptions = '';
		var map = '';
		
      if (ionic.Platform.isIPad()) 
		  $scope.GoogleMapStyle["height"] = 54 + "vh";
      else 
		  $scope.GoogleMapStyle["height"] = 80 + "vh";
	  
	  if (!window.localStorage.defaultMapLat)
		window.localStorage.defaultMapLat = "31.893451";
		
	  if (!window.localStorage.defaultMapLng)
		window.localStorage.defaultMapLng = "34.808019";
	
	  if (!window.localStorage.defaultMapZoom)
		window.localStorage.defaultMapZoom = 12;
	

	  if (!window.localStorage.mapStyleDefault)
		window.localStorage.mapStyleDefault = 'google.maps.MapTypeId.ROADMAP';


		 myLatlng = new google.maps.LatLng(window.localStorage.defaultMapLat,window.localStorage.defaultMapLng);
				 mapOptions = {
					center: myLatlng,
					zoom: parseInt(window.localStorage.defaultMapZoom),
					styles:[{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#C6E2FF"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#C5E3BF"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#D1D1B8"}]}],
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				 map = new google.maps.Map(document.getElementById("map"), mapOptions);
				map.setMapTypeId(eval(window.localStorage.mapStyleDefault));


    $scope.markers = [];
    var marker = "";
    $scope.markersArray = [];

    $scope.getAdminMap = function()
	{

        for (var i = 0; i < $scope.markersArray.length; i++ ) {
            $scope.markersArray[i].setMap(null);
        }
        $scope.markersArray.length = 0;


		$scope.sendparams = 
		{
			"user" : window.localStorage.userid,
			"pushid" : $rootScope.pushId,
		}



		SendPostToServer($scope.sendparams,$rootScope.LaravelHost+'/getAdminMap',function(data, success) 
		{		
			console.log("map users: " , data);

            $scope.mapArray = data;
			// Map Settings //
			$scope.initialise = function() {




				 //document.getElementById('maptype').value=map.getMapTypeId();

				
				
			  map.addListener('zoom_changed', function() {
				//$rootScope.defaultMapZoom = map.getZoom();
				window.localStorage.defaultMapZoom = map.getZoom();
			  });

			  map.addListener('maptypeid_changed', function() {
				var newMapType = 'google.maps.MapTypeId.'+map.getMapTypeId().toUpperCase();
				//$rootScope.mapStyleDefault = newMapType;
				window.localStorage.mapStyleDefault = newMapType;
			  });

			  
			  
			  

			  map.addListener('dragend', function() {
				//alert (map.getCenter())
				var positioncurrent = map.getCenter();
				//$rootScope.defaultMapLat = String(positioncurrent.lat());
				//$rootScope.defaultMapLng = String(positioncurrent.lng());
				window.localStorage.defaultMapLat = String(positioncurrent.lat());
				window.localStorage.defaultMapLng = String(positioncurrent.lng());
			  });
  
			  // Geo Location /
			  
				/*
				var circle = new google.maps.Circle({
					center: myLatlng,
					map: map,
					radius: $rootScope.DefaultRadius*800,          // IN METERS.
					fillColor: '#61B5CF',
					fillOpacity: 0.3,
					strokeColor: "#3b7791",
					strokeWeight: 2
				});
				 var direction = 1;
				var rMin = $rootScope.DefaultRadius*800, rMax = $rootScope.DefaultRadius*1000;
				setInterval(function() {
					var radius = circle.getRadius();
					if ((radius > rMax) || (radius < rMin)) {
						radius=rMin;
						//direction *= -1;
					}
					circle.setRadius(radius + direction * 10);
				}, 50);
				*/
				
				infowindow = new google.maps.InfoWindow({
					content: ''
				})

				


					/*
					map.setCenter(new google.maps.LatLng($scope.fields.location_lat,$scope.fields.location_lng));
					var myLocation = new google.maps.Marker({
						position: new google.maps.LatLng($scope.fields.location_lat,$scope.fields.location_lng),
						map: map,
						//http://maps.google.com/mapfiles/ms/icons/blue-dot.png
						icon : "img/icon-pin-location.png",
						animation: google.maps.Animation.DROP,
						title: "My Location"
					});
					*/

		
			  /*
				navigator.geolocation.getCurrentPosition(function(pos) {
				});
				*/
				var infowindows = [];
				$scope.map = map;
				// Additional Markers //

				/*
				var infoWindow = new google.maps.InfoWindow({
					content: 'Content goes here..'
				});
				*/

				var createMarker = function (index,info){
					
					if (info.image)
						$scope.markerImage = $scope.serverHost+info.image;
					else
						$scope.markerImage = "img/empty_logo.png";
					
					var prev_infowindow =false; 
					//alert (info);
					//alert (index);
					 marker = new google.maps.Marker({
					//var marker = new MarkerWithLabel({
					position: new google.maps.LatLng(info.current_lat, info.current_lng),
					//title : "123",
				//   labelContent: info.name,
				   labelAnchor: new google.maps.Point(40, 90),
				   labelClass: "labels", // the CSS class for the label
				   labelStyle: {opacity: 1},
					//infoWindowIndex: index,
					//visible:true,
					map: $scope.map,
					//icon : $rootScope.PHPHost+info.image,
					tooltip: '<B>test</B>',
					icon: new google.maps.MarkerImage(
					$scope.markerImage, // my 16x48 sprite with 3 circular icons
					null, // desired size
					null, // offset within the scaled sprite
					null, // anchor point is half of the desired size
					new google.maps.Size(60, 60) // scaled size of the entire sprite
				   ),
					optimized:false,
				
					//shape:{coords:[17,17,18],type:'circle'},
					//animation: google.maps.Animation.DROP,
					//title: info.name
					});

					
					 // I create an OverlayView, and set it to add the "markerLayer" class to the markerLayer DIV
					 var myoverlay = new google.maps.OverlayView();
					 myoverlay.draw = function () {
						 this.getPanes().markerLayer.id='markerLayer';
					 };
					 myoverlay.setMap(map);

		 
					//var htmlElement = '<div class="infowindow">sadad</div>'
					//var compiled = $compile(htmlElement)($scope);


					marker.content = '<div class="infoWindowContent" >test</div>';

				
					var content = '<div ng-click="navigateDetails('+index+')">'+
					'<h1>test '+info.name+'</h1>'+
					'<div>'+info.catagoryname+'</div>'+
					'<div ><img src="'+$rootScope.PHPHost+info.image+'" style="width:60px; height:60px;"></div>'+
					'</div>';
					
					//var compiledContent = $compile(content)($scope)


                    $scope.markersArray.push(marker);
					google.maps.event.addListener(marker, 'click', function() 
					{
						alert (info.name);
						/*
						$scope.fields.showdetails = true;	
						$scope.fields.supplierindex = index;	
						$scope.showSupplierPopup(index,info);
						$scope.$apply();					
						*/
					});

				}  
				
				$scope.isFirstMarker = 0;
				
				console.log("$scope.mapArray",$scope.mapArray);



                for(var i=0;i< $scope.mapArray.length;i++)
				{
						createMarker(i,$scope.mapArray[i]);
				}	


			};
			
			
			google.maps.event.addDomListener(document.getElementById("map"), 'load', $scope.initialise());
		});			
	}
			/*
			 $rootScope.Mapinterval = $interval(function() {
				$scope.getAdminMap();
			 }, 20000);	
			 */
			 
			 $rootScope.Mapinterval = setInterval($scope.getAdminMap, 10000);


			$scope.getAdminMap();		
			
	$scope.$on("$ionicView.afterLeave", function(event, data){
	   
		clearInterval($rootScope.Mapinterval);
	});	
	

})
.filter('filterByStatus', function(){
    return function( items, tab ) {
      var filtered = [];
	  
	for(var x = 0; x < items.length; x++)
	{
		if (items[x].status != 3 && tab == "0")
			filtered.push(items[x]);
		
		if (items[x].status ==3 && tab == "1")
			filtered.push(items[x]);
	}		

      return filtered;
    };
})

.filter('adminMessagesFilter', function(){
    return function( items, tab ) {
      var filtered = [];

		for(var x = 0; x < items.length; x++)
		{
			
			if (items[x].info[0].type == 1 && tab == "0")
				filtered.push(items[x]);
			
			if (items[x].info[0].type == 0 && tab == "1")
				filtered.push(items[x]);
			
			if (items[x].info[0].type == 3 && tab == "2")
				filtered.push(items[x]);		
		}		  

      return filtered;
    };
})


.controller('DemoCtrl', function($scope, $stateParams,$ionicPopup,$localStorage,$rootScope,SendPostToServer,$ionicScrollDelegate,$timeout,$ionicModal,$ionicPlatform,$cordovaCamera,$ionicHistory,$state,$cordovaGeolocation,$interval,$ionicLoading) {

	$scope.navTitle = "<p dir='rtl'>הדגמה לקוח פרטי</p>";
	$scope.ActiveTab = 3;
	$scope.informationArray = [];
	
	
	$scope.setActiveTab = function(tab)
	{
		$scope.ActiveTab = tab;
	}
	
	$rootScope.$watch('settingsArray', function () 
	{   
		$scope.informationArray = $rootScope.settingsArray;
		
		//if ($rootScope.settingsArray[0])
			//$scope.deliveryTime = $rootScope.settingsArray[0].delivery_time;
	});

	
	
	$scope.showInformationPopup = function()
	{
	   $ionicModal.fromTemplateUrl('templates/information_popup.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(informationPopup) {
		  $scope.informationPopup = informationPopup;
		  $scope.informationPopup.show();
		});
	}
	
	$scope.closeInformationPopup = function()
	{
		$scope.informationPopup.hide();
	}	
	
	$scope.showLoginOptions = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'יש תחילה להתחבר או להרשם',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [

		{
		text: "ביטול",
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	   {
		text: "התחברות/הרשמה",
		type: 'button-positive',
		onTap: function(e) { 
			window.location ="#/app/login";
		}
	   },	   
	   ]
	  });		  
	}

})

	


.directive('focusMe', function($timeout) {
  return {
    link: function(scope, element, attrs) {

      $timeout(function() {
        element[0].focus(); 
      });
    }
  };
})
